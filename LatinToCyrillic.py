# -*- coding: utf8 -*-
import nltk
from nltk.corpus.reader import TaggedCorpusReader
reader = TaggedCorpusReader('.', ['corpus.txt'])

my_tagged_sents=reader.tagged_sents()
my_sents=reader.sents()

FILEIN = open("inputText.txt", "r")
data = FILEIN.read()
FILEIN.close()
size = int(len(my_tagged_sents))

train_sents = my_tagged_sents[:size]

tnt_tagger = nltk.tag.tnt.TnT()
tnt_tagger.train(train_sents)

FILEOUT = open("answer.txt", "w")
for i in data.split(' '):
    FILEOUT.writelines(str(tnt_tagger.tag(i)))
FILEOUT.close()
